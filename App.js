import React, { Component } from 'react';
import {
  StackNavigator,
} from 'react-navigation';

import CharactersList from './CharactersList'
import CharacterDetail from './CharacterDetail'
import Map from './Map'

const Stacks = StackNavigator({
  Home: { screen: CharactersList },
  CharacterDetail: { screen: CharacterDetail },
  Location: { screen: Map },
});

export default class App extends Component {
  render() {
    return (
      <Stacks />
    );
  }
}