import React from 'react';
import CharList, { prepareCharacters, getCharacters } from './CharactersList';

import renderer from 'react-test-renderer';
EMPTY_CHAR = []
EMPTY_CHAR_RESPONSE_FROM_SERVER = JSON.stringify({ count: 0, next: null, results: EMPTY_CHAR })

CHARS = [
  { name: "Luke" },
  { name: "Leia" },
  { name: "R2D2" },
]
CHARS_RESPONSE_FROM_SERVER = JSON.stringify({ count: 0, next: null, results: CHARS })


CHARS2 = [
  { name: "Darth Vador" },
  { name: "Anakin" },
  { name: "Toto" },
]
CHARS_RESPONSE_FROM_SERVER_WITH_NEXT = JSON.stringify({ count: 0, next: "https://swapi.co/api/people?page=2", results: CHARS })

describe('testing the home page', () => {
  beforeEach(() => {
    fetch.resetMocks()
  })

  it('renders without crashing', () => {
    // given
    fetch.mockResponseOnce(EMPTY_CHAR_RESPONSE_FROM_SERVER)

    // when
    const rendered = renderer.create(<CharList navigation={{}}/>).toJSON();

    // then
    expect(rendered).toBeTruthy();
  });
})


describe('unit tests, CharList.js', () => {
  describe('prepareCharacters', () => {
    it('returns empty list if no chars', () => {
      // given

      // when
      const chars = prepareCharacters(EMPTY_CHAR)

      // then
      expect(chars).toHaveLength(0)
    });

    it('format and sort data to be presented', () => {
      // given

      // when
      const chars = prepareCharacters(CHARS)

      // then
      expect(chars).toHaveLength(2)
      expect(chars).toEqual([
        {
          title: 'L',
          data: [
            { name: 'Leia'},
            { name: 'Luke'},
          ]
        },
        {
          title: 'R',
          data: [
            { name: 'R2D2'},
          ]
        }
      ])
    });
  })

  describe('getCharacters', () => {
    beforeEach(() => {
      fetch.resetMocks()
    })
    it('returns the chars list', () => {
      // given
      fetch.mockResponseOnce(CHARS_RESPONSE_FROM_SERVER)

      // when
      return getCharacters().then(chars => {

        // then
        expect(chars).toHaveLength(3)
        expect(chars).toEqual(CHARS)
      })
    });

    it('returns the chars from paginated response', () => {
      // given
      fetch
        .once(CHARS_RESPONSE_FROM_SERVER_WITH_NEXT)
        .once(CHARS_RESPONSE_FROM_SERVER)

      // when
      return getCharacters().then(chars => {

        // then
        expect(chars).toHaveLength(6)
      })
    });
  })
})

