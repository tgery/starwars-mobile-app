import React from 'react';
import App from './App';

import renderer from 'react-test-renderer';
EMPTY_CHAR = []
EMPTY_CHAR_RESPONSE_FROM_SERVER = JSON.stringify({ count: 0, next: null, results: EMPTY_CHAR })

describe('testing the home page', () => {
  beforeEach(() => {
    fetch.resetMocks()
  })

  it('renders without crashing', () => {
    // given
    fetch.mockResponseOnce(EMPTY_CHAR_RESPONSE_FROM_SERVER)

    // when
    const rendered = renderer.create(<App navigation={{}}/>).toJSON();

    // then
    expect(rendered).toBeTruthy();
  });
})
