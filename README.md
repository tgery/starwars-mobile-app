# Star wars explorer

[![CircleCI](https://circleci.com/bb/tgery/starwars-mobile-app/tree/master.svg?style=svg)](https://circleci.com/bb/tgery/starwars-mobile-app/tree/master)


This a demo application to discover the react native ecosystem. It list the star wars characters. It also include a map.

You can access the application using [expo](https://expo.io/@tgery/snooper-react-native)

## Requirements

* NodeJS for development
* expo application

## Commands

```sh
npm test # run unit tests
npm start # start local server (then scan QR code with expo application)
```

## TODO
 * load data in parallel at start up
 * start to show data at once the first HTTP request is completed
 * Add some styling on the detail page