import React, { Component } from 'react';
import { AppRegistry, SectionList, StyleSheet, Text, View } from 'react-native';
import SvgUri from 'react-native-svg-uri';

export default class CharactersDetail extends Component {
  static navigationOptions = {
    title: 'Characters detail',
  };

  constructor(props) {
    super(props);
  }

  getCharacter() {
    return this.props.navigation.state.params.character;
  }

  render() {
    const char = this.getCharacter()
    const gender = char.gender == 'male' ? 'mars' : 'venus'
    const imgUri = 'https://use.fontawesome.com/releases/v5.0.8/svgs/solid/' + gender + '.svg'

    return (
      <View style={styles.container}>
        <View style={styles.row}>
          <SvgUri
            width="30"
            height="30"
            source={ {uri: imgUri} }
          />
          <Text style={styles.name}> {char.name} </Text>
        </View>
        <View style={styles.row}>
          <Text style={styles.property}> 	<Text style={styles.label}>Height:</Text> {char.height} cm</Text>
          <Text style={styles.property}> 	<Text style={styles.label}>Mass:</Text> {char.mass} kg</Text>
        </View>
        <View style={styles.row}>
          <Text style={styles.property}> 	<Text style={styles.label}>Birth Year:</Text> {char.birth_year} </Text>
        </View>
        <View style={styles.row}>
          <Text style={styles.property}> 	<Text style={styles.label}>Hair Color:</Text> {char.hair_color} </Text>
          <Text style={styles.property}> 	<Text style={styles.label}>Skin Color:</Text> {char.skin_color} </Text>
          <Text style={styles.property}> 	<Text style={styles.label}>Eye Color:</Text> {char.eye_color} </Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    padding:10,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
  },
  row: {
    flex: 1,
    flexDirection: 'row',
    padding: 5,
    margin: 5,
    borderBottomWidth: 1,
    borderColor: '#d6d7da',
    flexWrap: 'wrap',
  },
  name: {
    fontSize: 30,
  },
  property: {
    fontSize: 20,
  },
  label: {
    fontWeight: 'bold',
  }
})