import React from 'react';
import MapView, { Marker } from 'react-native-maps';
import { Text, View } from 'react-native';


export default class Map extends React.Component {

  constructor(props, context) {
    super(props, context);

    this.state = {
      region: {
        latitude: -33.8688,
        longitude: 151.2093,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
      },
      markers: [
        {
          key: '1',
          latlng: {
            latitude: -33.8788,
            longitude: 151.2493,
          },
          title: 'The first point',
          description: 'Awesome, right?',
        },
        {
          key: '2',
          latlng: {
            latitude: -33.8688,
            longitude: 151.2093,
          },
          title: 'The second point',
          description: 'Stiff awesome, right?',
        },
        {
          key: '3',
          latlng: {
            latitude: -33.2688,
            longitude: 151.2793,
          },
          title: 'Another one',
          description: 'Boring...',
        },
        {
          key: '4',
          latlng: {
            latitude: -33.8658,
            longitude: 151.2493,
          },
          title: 'yet a new point',
          description: 'When does it end?',
        },
      ]
    }
    this.getCurrentLoc();
  }

  getCurrentLoc() {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        this.setState(old => {
          old.region = {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
          }
          return old
        });
      },
      (error) => Alert.alert(
        'Impossible to retrieve your location',
        'Err: ' + JSON.stringify(e),
      ),
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
    );
  }

  render() {
    return (
      <MapView
        style={{ flex: 1 }}
        region={this.state.region}>
        {this.state.markers.map(marker => (
          <Marker
            coordinate={marker.latlng}
            title={marker.title}
            description={marker.description}
          />
        ))}
      </MapView>
    );
  }
}