import React, { Component } from 'react';
import { AppRegistry, SectionList, StyleSheet, Text, View, TouchableOpacity, Button,Alert } from 'react-native';


export default class CharactersList extends Component {
  static navigationOptions = {
    title: 'Home',
  };

  constructor(props) {
    super(props);
    this.state = { characters: [] };
    getCharacters()
      .then(prepareCharacters)
      .then(chars => {
        const newState = { characters: chars }
        this.setState(previousState => newState)
      })
      .catch(e => Alert.alert(
        'Impossible to retrieve the data',
        'Err: ' + JSON.stringify(e),
      ))
  }

  render() {
    const { navigate } = this.props.navigation;

    return (
      <View style={styles.container}>
        <Button
            onPress={() =>
              navigate('Location')} 
            title="Locate your self"
            color="#841584" />
        <SectionList
          sections={this.state.characters}
          renderItem={({ item }) =>  
          <TouchableOpacity
            style={styles.button} 
            onPress={() =>
              navigate('CharacterDetail', { character: item })} >
            <Text style={styles.item} >{item.name}</Text>
          </TouchableOpacity>}
          renderSectionHeader={({ section }) => <Text style={styles.sectionHeader}>{section.title}</Text>}
          keyExtractor={(item, index) => index}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  sectionHeader: {
    paddingTop: 2,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 2,
    fontSize: 14,
    fontWeight: 'bold',
    backgroundColor: 'rgba(247,247,247,1.0)',
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
    borderBottomWidth: 1,
    borderColor: 'white',
  },
})

export function getCharacters(url = 'https://swapi.co/api/people', othersChars = []) {
  return fetch(url)
    .then(function (response) {
      if (response.status !== 200) {
        return Promise.reject('HTTP response failed: ' + response)
      }
      return response.json();
    })
    .then(response => {
      const allChars = response.results.concat(othersChars)
      if (response.next == null) {
        return allChars
      } else {
        return getCharacters(response.next, allChars)
      }
    })
    .catch(error => Promise.reject(error))
}

export function prepareCharacters(charsList) {
  function groupByFirstLetter(acc, val) {
    const firstLetter = val['name'][0]
    if (!Object.keys(acc).includes(firstLetter)) {
      acc[firstLetter] = []
    }
    acc[firstLetter].push(val)
    return acc
  }

  charsAsObject = charsList
    .reduce(groupByFirstLetter, {})

  const data = Object.keys(charsAsObject).map(key => {
    return { 
      title: key, 
      data: charsAsObject[key] 
    }
  }).sort((a, b) => a.title > b.title)

  data.map(row => {
    row.data = row.data.sort((a, b) => a.name > b.name)
  })

  return data;
}